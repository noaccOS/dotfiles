local wezterm = require 'wezterm';
return {
   color_scheme   = "nord",
   enable_tab_bar = false,
   -- enable_wayland = true,
   font           = wezterm.font_with_fallback({
         "JetBrains Mono",
	 "JetBrainsMono Nerd Font",
	 "Segoe UI Symbol",
	 "Cambria",
	 "JoyPixels"
   }),
   font_size      = 12,
   window_padding = {
      left   = 20,
      right  = 20,
      top    = 20,
      bottom = 20
   },
}
