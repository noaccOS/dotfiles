function fish_greeting
    set term (basename "/"(ps -f -p (cat /proc/(echo %self)/stat | cut -d \  -f 4) | tail -1 | sed 's/^.* //'))
    if test "$term" = "kitty"
        neofetch --config ~/.config/neofetch/config-kitty.conf
    else if test "$term" = "wezterm-gui"
	neofetch --config ~/.config/neofetch/config-wezterm.conf
    else
        neofetch --config ~/.config/neofetch/config-others.conf
    end
end
