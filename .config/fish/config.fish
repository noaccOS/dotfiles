alias em='emacsclient -c'
alias emt='emacsclient -t'
alias vim='emacsclient -t'

# Yay command
alias yup='yay -Syu --overwrite \*'
alias yin='yay -S --overwrite \*'
alias yrm='yay -Rsnc'

# Paru commands
alias pup='paru -Syu --overwrite \*'
alias pin='paru -S --overwrite \*'
alias prm='paru -Rsnc'

# Commands redefinition
alias clear='fish_greeting'
alias rm='rm -r'
alias cp='cp -r'
alias less='bat'
alias cat='bat'

# Colors in default applications
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias ip='ip -color=auto'

# Permit other users to launch applications, like when sudoing
xhost + >/dev/null

# Start X at login
if status is-login
    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        exec startx -- -keeptty
    end
end

thefuck --alias | source
test -e /etc/profile.d/nix.sh && fenv source /etc/profile.d/nix{,-daemon}.sh
test -r ~/.dir_colors && eval (dircolors ~/.dir_colors | sed 's/LS_COLORS=/set LS_COLORS /g')
