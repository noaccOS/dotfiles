;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Francesco Noacco"
      user-mail-address "francesco.noacco2000@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "JetBrainsMono Nerd Font" :size 18))
;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-nord)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.

(setq-default cursor-type 'bar)

;; Company configuration
(with-eval-after-load 'company
    (setq company-idle-delay 0
          company-minimum-prefix-length 1
          company-auto-complete t
          company-require-match nil
          company-auto-complete nil))

(require 'company-files)

(dolist (extension '(".fdb_latexmk" ".aux" ".log" ".pdf" ".bbl" ".bcf" ".gz" ".blg" ".fls"))
    (push extension company-files-exclusions))


;; EAF Configuration
(setq eaf-config-location "~/.emacs.d/.local/cache/eaf/")

;; Ligatures support for JetBrains Mono
(let ((alist '((?! . "\\(?:!\\(?:==\\|[!=]\\)\\)")
              (?# . "\\(?:#\\(?:###?\\|_(\\|[!#(:=?[_{]\\)\\)")
              (?$ . "\\(?:\\$>\\)")
              (?& . "\\(?:&&&?\\)")
              (?* . "\\(?:\\*\\(?:\\*\\*\\|[/>]\\)\\)")
              (?+ . "\\(?:\\+\\(?:\\+\\+\\|[+>]\\)\\)")
              (?- . "\\(?:-\\(?:-[>-]\\|<<\\|>>\\|[<>|~-]\\)\\)")
              (?. . "\\(?:\\.\\(?:\\.[.<]\\|[.=?-]\\)\\)")
              (?/ . "\\(?:/\\(?:\\*\\*\\|//\\|==\\|[*/=>]\\)\\)")
              (?: . "\\(?::\\(?:::\\|\\?>\\|[:<-?]\\)\\)")
              (?\; . "\\(?:;;\\)")
              (?< . "\\(?:<\\(?:!--\\|\\$>\\|\\*>\\|\\+>\\|-[<>|]\\|/>\\|<[<=-]\\|=\\(?:=>\\|[<=>|]\\)\\||\\(?:||::=\\|[>|]\\)\\|~[>~]\\|[$*+/:<=>|~-]\\)\\)")
              (?= . "\\(?:=\\(?:!=\\|/=\\|:=\\|=[=>]\\|>>\\|[=>]\\)\\)")
               (?> . "\\(?:>\\(?:=>\\|>[=>-]\\|[]:=-]\\)\\)")
               (?? . "\\(?:\\?[.:=?]\\)")
              (?\[ . "\\(?:\\[\\(?:||]\\|[<|]\\)\\)")
              (?\ . "\\(?:\\\\/?\\)")
              (?\] . "\\(?:]#\\)")
              (?^ . "\\(?:\\^=\\)")
              (?_ . "\\(?:_\\(?:|?_\\)\\)")
              (?{ . "\\(?:{|\\)")
              (?| . "\\(?:|\\(?:->\\|=>\\||\\(?:|>\\|[=>-]\\)\\|[]=>|}-]\\)\\)")
              (?~ . "\\(?:~\\(?:~>\\|[=>@~-]\\)\\)"))))
 (dolist (char-regexp alist)
   (set-char-table-range composition-function-table (car char-regexp)
                         `([,(cdr char-regexp) 0 font-shape-gstring]))));

;; Custom package definition
(use-package! eaf
  :custom
  (eaf-find-alternate-file-in-dired t)
  :config
  (eaf-bind-key scroll_up "RET" eaf-pdf-viewer-keybinding)
  (eaf-bind-key scroll_down_page "DEL" eaf-pdf-viewer-keybinding)
  (eaf-bind-key scroll_up "C-n" eaf-pdf-viewer-keybinding)
  (eaf-bind-key scroll_down "C-p" eaf-pdf-viewer-keybinding)
  (eaf-bind-key take_photo "p" eaf-camera-keybinding))

(use-package! lsp-java
  :config (add-hook 'java-mode-hook 'lsp))

(use-package! lsp-haskell
 :ensure t
 :config
 (setq lsp-haskell-process-path-hie "haskell-language-server-wrapper")
 ;; Comment/uncomment this line to see interactions between lsp client/server.
 ;;(setq lsp-log-io t)
)

;; Elcord configuration
(setq elcord-use-major-mode-as-main-icon t)

;; Org
(setq-default
       org-highlight-latex-and-related '(native script entities)
       org-pretty-entities t
       org-pretty-entities-include-sub-superscripts t)

(add-hook! 'org-mode-hook 'org-fragtog-mode)

;; Keybindings
(load! "keybindings.el")

;; Private variables, like github token
(when (f-exists? "private.el") (load! "private.el"))
(put 'dired-find-alternate-file 'disabled nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(eaf-find-alternate-file-in-dired t t)
 '(package-selected-packages '(lsp-haskell)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
