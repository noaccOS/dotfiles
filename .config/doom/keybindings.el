;;; ~/.doom.d/keybindings.el -*- lexical-binding: t; -*-

;; Custom keybindings for my config

;; Movement - Beware that I use dvorak
(define-key key-translation-map (kbd "C-?") (kbd "C-h"))
(define-key key-translation-map (kbd "C-p") (kbd "C-s"))
(define-key key-translation-map (kbd "C-b") (kbd "C-t"))

(define-key key-translation-map (kbd "M-b") (kbd "M-h"))
(define-key key-translation-map (kbd "M-f") (kbd "M-s"))

(define-key key-translation-map (kbd "C-h") (kbd "C-b"))
(define-key key-translation-map (kbd "C-t") (kbd "C-n"))
(define-key key-translation-map (kbd "C-n") (kbd "C-p"))
(define-key key-translation-map (kbd "C-s") (kbd "C-f"))

(define-key key-translation-map (kbd "M-h") (kbd "M-b"))
(define-key key-translation-map (kbd "M-s") (kbd "M-f"))

;; Dvorak C-x C-u
(define-key key-translation-map (kbd "C-x") (kbd "C-u"))
(define-key key-translation-map (kbd "C-u") (kbd "C-x"))

;; Doom Emacs
(setq doom-leader-alt-key "C-.")
(setq doom-localleader-alt-key "C-. l")

;; Swiper
;;(global-set-key "\C-s" 'swiper)

;; Company
(with-eval-after-load 'company
  (define-key company-active-map (kbd "<return>") nil)
  (define-key company-active-map (kbd "<tab>") nil)
  (define-key company-active-map (kbd "RET") nil)
  (define-key company-active-map (kbd "SPC") nil)
  (define-key company-active-map (kbd "C-SPC") #'company-complete-selection)
  (define-key company-mode-map [remap indent-for-tab-command] #'company-indent-or-complete-common))
